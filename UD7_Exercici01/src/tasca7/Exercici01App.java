package tasca7;

import java.util.Hashtable;
import java.util.ArrayList;
import java.util.Scanner;

public class Exercici01App {

	public static void main(String[] args) {
		// Aplicaci� que calcula la nota mitja dels alumnes	de la classe de programaci�	
		Scanner scan = new Scanner (System.in);
		System.out.println("Introdueix el nombre d'alumnes de la classe de programaci�:");
		int alumnes = scan.nextInt();
		System.out.println("Introdueix el nombre d'assignatures que fa cada alumne:");
		int assignatura = scan.nextInt();
		scan.close();
		Hashtable <String, ArrayList <Float>> notes = notesAlumnes (alumnes, assignatura);
		System.out.println(mitjana (notes,alumnes, assignatura));
		
	}
	public static Hashtable <String, ArrayList <Float>>  notesAlumnes (int alumnes, int assignatures) {
		// Generem les notes dels alumnes amb valors aleat�ris i les introdu�m a un diccionari
		Hashtable <String,ArrayList <Float>> notes = new Hashtable <String,ArrayList <Float>>();
		System.out.println("Les notes dels alumnes s�n:");
		for (int i = 1; i <= (alumnes);i++) {
			String alumne = "alumne "+ i;
			ArrayList<Float> notesAssig = new ArrayList<>();			
			notes.put(alumne, notesAssig);			
			for ( int j = 1; j <= assignatures; j++) {

				float nota = (float) Math.random()*10 ;
				
				notesAssig.add(nota);
			}	
		}	
		System.out.println(notes);
		return notes;
	}
	public static Hashtable <String,Float>  mitjana (Hashtable <String, ArrayList <Float>>  mitja, int alumnes, int assignatures) {
		// Calculem la nota mitjana de cada alumne i la introdu�m a un diccionari
		Hashtable <String,Float> mitjanaAlumnes = new Hashtable <String,Float>();	
		float suma, mitjana;
		System.out.println("La nota mitja de cada alumne �s:");
		ArrayList <Float> mitjanaList = new ArrayList<>(); 
		for (int i = 1; i <= (alumnes);i++) {
			String alumne = "alumne "+ i;
			suma = 0;
			for ( int j = 0; j < assignatures; j++) {
				mitjanaList = mitja.get(alumne);			
				suma = suma + mitjanaList.get(j);
			}
			mitjana = suma/assignatures;
			mitjanaAlumnes.put(alumne, mitjana);
		}
		
		return mitjanaAlumnes;
	}

}
