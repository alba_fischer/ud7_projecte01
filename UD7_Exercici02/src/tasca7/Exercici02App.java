package tasca7;

import java.util.Scanner;
import java.util.ArrayList;

public class Exercici02App {

	public static void main(String[] args) {
		/* Creem una alicaci� que gestioni el flux de ventes d'un 
		 * supermercat. 
		 * El programa finalitza quan l'usuari no vol comprar res m�s.
		 */
		System.out.println("Benvingut al supermercat!");
		venta ();		
	}
	
	public static void venta() {
		//L'usuari ens indica quan val cada producte, l'Iva aplicat, els diners que d�na per pagar i quan ja no t� m�s productes al carro.
		boolean finalCompra=false;
		ArrayList<Double> carrito = new ArrayList<>();
		Scanner scan = new Scanner (System.in);	
		int cont = 0;
		do {						
			System.out.println("Quant val aquest producte? (Useu coma, no punt)");
			double preuProduct = scan.nextDouble();
			carrito.add(preuProduct); 
			System.out.println("Vols comprar algun producte m�s? (Respon si o no)");
			String resposta = scan.next();
			if (resposta.equals("no")) {
				finalCompra = true;
			}
			cont  = cont + 1;
		}while(finalCompra == false);
		
		double preuTotalBrut = 0;
		for (int i = 0; i < cont; i++) {
			preuTotalBrut = preuTotalBrut + carrito.get(i);
		}
		System.out.println("Introdu�u l'IVA aplicat (21 o 4 %):");
		int iva = scan.nextInt();
		double preuTotal = preuTotalBrut + preuTotalBrut*((double)iva/(double)100);
		
		//Determinem el preu total, introdu�m la quantitat pagada pel client i determinem el canvi que ha de rebre el client
		System.out.println("El preu total a pagar �s: " + preuTotal);
		System.out.println("Quantitat pagada pel client:");
		double pago = scan.nextInt();
		double canvi = pago - preuTotal;
		System.out.println("Moltes gr�cies! Aqu� tens el teu canvi:" + canvi);
		scan.close();
		
		//Mostrem els valors per pantalla
		System.out.println(" ");
		System.out.println("IVA aplicat:" + iva);
		System.out.println("Preu total brut:" + preuTotalBrut);
		System.out.println("Preu total + IVA:" + preuTotal);
		System.out.println("N�mero d'articles comprats:"+ cont);
		System.out.println("Quantitat pagada:" + pago);
		System.out.println("Canvi a tornar al client:" + canvi);
	}
	
}
