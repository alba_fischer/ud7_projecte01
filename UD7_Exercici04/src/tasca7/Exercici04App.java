package tasca7;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Scanner;

public class Exercici04App {

	public static void main(String[] args) {
		/* Creem una aplicaci� que t� una base de dades on pots afegir productes, consultar el preu o mostrar tota la informaci�.
		 * Tamb� gestiona ventes. 
		 */
		Scanner scan = new Scanner (System.in);
		Hashtable <String, Double> stock = baseDeDades();
		boolean control = false;
		
		
		System.out.println("Bon dia! Quina operaci� li agradaria fer; comprar, afegir, consultar o mostrar?");
		
		
		do {
			String operacio = scan.next();
			switch (operacio) {
				case "comprar" :
					// gestionem les ventes
					System.out.println("Primer de tot li mostrem els productes en Stock:");
					mostrarDades(stock);
					boolean finalCompra=false;
					int cont = 0;
					double preuTotalBrut = 0;
					/*Interaccionem amb l'usuari i fins que aquest no diu que ja no vol comprar res, no deixem de demanar-li 
					 * productes i sumar-los al total
					 */
					do {
						System.out.println("Quin producte t'agradaria comprar?");
						String producte = scan.next();
						stock.get(producte);
						preuTotalBrut = preuTotalBrut + stock.get(producte);
						System.out.println("Vols comprar algun producte m�s? (Respon si o no)");
						String resposta = scan.next();
						if (resposta.equals("no")) {
							finalCompra = true;
						}
						cont  = cont + 1;
					}while (finalCompra == false);
					System.out.println("Introdu�u l'IVA aplicat (21 o 4 %):");
					int iva = scan.nextInt();
					double preuTotal = preuTotalBrut + preuTotalBrut*((double)iva/(double)100);
					//Determinem el preu total, introdu�m la quantitat pagada pel client i determinem el canvi que ha de rebre el client
					System.out.println("El preu total a pagar �s: " + preuTotal);
					System.out.println("Quantitat pagada pel client:");
					double pago = scan.nextInt();
					double canvi = pago - preuTotal;
					System.out.println("Moltes gr�cies! Aqu� tens el teu canvi:" + canvi);
					
					//Mostrem els valors de la compra per pantalla
					System.out.println(" ");
					System.out.println("Els resultats d'aquesta compra s�n:");
					System.out.println("IVA aplicat: " + iva);
					System.out.println("Preu total brut: " + preuTotalBrut);
					System.out.println("Preu total + IVA: " + preuTotal);
					System.out.println("N�mero d'articles comprats: "+ cont);
					System.out.println("Quantitat pagada: " + pago);
					System.out.println("Canvi a tornar al client: " + canvi);
					break;
				case "afegir":
					// L'usuari afegeix productes a la base de dades
					System.out.println("Que t'agradaria afegir? Indica el nom del producte:");
					String clau = scan.next();
					System.out.println("Indica el preu del producte (Useu coma, no punt):");
					double preu = scan.nextDouble();
					stock = afegirDades(stock, clau, preu);
					break;
				case "consultar":
					// L'usuari pot consultar qualsevol producte
					System.out.println("Quin preu voldries consultar? Indica el nom del producte:");
					String producte = scan.next();
					consultarDades(stock, producte);
					break;
				case "mostrar":
					// L'usuari pot demanar que li mostrin tota la informaci� de la base de dades 
					mostrarDades(stock);
					break;
				
			}
			
			System.out.println("Li agradaria fer alguna altra operaci�? (Respon si o no)");
			String resposta  = scan.next();
			if (resposta.equals("no")) {
				control = true;
			} else {
				System.out.println("Quina altra operaci� li agradaria fer; comprar, afegir, consultar o mostrar?");
			}
		}while(control == false);
	scan.close();
	System.out.println("Vale, fins aviat!");
	}

	public static Hashtable <String, Double>  baseDeDades() {
		// Creem el diccionari que ser� la base de dades
		Hashtable <String, Double> stock = new Hashtable <String,Double>();
		
		stock.put("xocolata", 1.2);
		stock.put("aigua", 0.5);
		stock.put("galletes", 2.0);
		stock.put("fruita", 2.5);
		stock.put("saltxitxa", 3.8);
		stock.put("llom", 4.0);
		stock.put("iogurt", 4.3);
		stock.put("llet", 0.9);
		stock.put("pernil", 2.35);
		stock.put("formatge", 2.1);
		
		return stock;
	}
	public static Hashtable <String, Double>  afegirDades(Hashtable <String, Double> stock, String clau, double preu) {
		// El client afegeix nous productes amb el seu respectiu preu a la base de dades
		stock.put(clau, preu);
		return stock ;
	}
	public static void  consultarDades(Hashtable <String, Double> stock, String clau) {
		// El client pot consultar el preu de qualsevol producte
		System.out.println("El preu del/de la " + clau + " �s " + stock.get(clau) + "�");
	}
	public static void  mostrarDades(Hashtable <String, Double> stock) {
		// El client pot demanar que se li mostrin tots els productes que estan en stock
		Enumeration<Double> preus = stock.elements();
		Enumeration<String> productes = stock.keys();
		while (preus.hasMoreElements()) {
			System.out.println("El preu del/de la " + productes.nextElement() + " �s " + preus.nextElement() + "�");
		}
	}
	
}
