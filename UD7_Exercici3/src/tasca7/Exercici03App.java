package tasca7;

import java.util.Scanner;
import java.util.Enumeration;
import java.util.Hashtable;

public class Exercici03App {

	public static void main(String[] args) {
		/* Creem una base de datos amb art�cles per controlar l'stock de productes.
		 * L'usuari pot llistar, afegir i consultar informaci�. Tamb� ens diu si vol fer una altra operaci� o si no.
		 * El programa finalitza quan l'usuari ja no vol fer cap altra operaci�.
		 */
		Scanner scan = new Scanner (System.in);
		Hashtable <String, Double> stock = baseDeDades();
		boolean control = false;
		
		System.out.println("Bon dia! Quina operaci� li agradaria fer; afegir, consultar o mostrar?");
		
		
		do {
			String operacio = scan.next();
			switch (operacio) {
				case "afegir":
					System.out.println("Que t'agradaria afegir? Indica el nom del producte:");
					String clau = scan.next();
					System.out.println("Indica el preu del producte (Useu coma, no punt):");
					double preu = scan.nextDouble();
					stock = afegirDades(stock, clau, preu);
					break;
				case "consultar":
					System.out.println("Quin preu voldries consultar? Indica el nom del producte:");
					String producte = scan.next();
					consultarDades(stock, producte);
					break;
				case "mostrar":
					mostrarDades(stock);
					break;
				
			}
			
			System.out.println("Li agradaria fer alguna altra operaci�? (Respon si o no)");
			String resposta  = scan.next();
			if (resposta.equals("no")) {
				control = true;
			} else {
				System.out.println("Quina altra operaci� li agradaria fer; afegir, consultar o mostrar?");
			}
		}while(control == false);
	scan.close();
	System.out.println("Vale, fins aviat!");
	}
		
	public static Hashtable <String, Double>  baseDeDades() {
		// Creem el diccionari que ser� la base de dades
		Hashtable <String, Double> stock = new Hashtable <String,Double>();
		
		stock.put("xocolata", 1.2);
		stock.put("aigua", 0.5);
		stock.put("galletes", 2.0);
		stock.put("fruita", 2.5);
		stock.put("saltxitxa", 3.8);
		stock.put("llom", 4.0);
		stock.put("iogurt", 4.3);
		stock.put("llet", 0.9);
		stock.put("pernil", 2.35);
		stock.put("formatge", 2.1);
		
		return stock;
	}
	
	public static Hashtable <String, Double>  afegirDades(Hashtable <String, Double> stock, String clau, double preu) {
		// El client afegeix nous productes amb el seu respectiu preu a la base de dades
		stock.put(clau, preu);
		return stock ;
	}
	public static void  consultarDades(Hashtable <String, Double> stock, String clau) {
		// El client pot consultar el preu de qualsevol producte
		System.out.println("El preu del/de la " + clau + " �s " + stock.get(clau) + "�");
	}
	public static void  mostrarDades(Hashtable <String, Double> stock) {
		// El client pot demanar que se li mostrin tots els productes que estan en stock
		Enumeration<Double> preus = stock.elements();
		Enumeration<String> productes = stock.keys();
		while (preus.hasMoreElements()) {
			System.out.println("El preu del/de la " + productes.nextElement() + " �s " + preus.nextElement() + "�");
		}
	}
}
